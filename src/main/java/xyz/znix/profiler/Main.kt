package xyz.znix.profiler

import java.awt.Dimension
import java.io.File
import java.io.RandomAccessFile
import javax.swing.JFileChooser
import javax.swing.filechooser.FileFilter
import javax.swing.text.DefaultCaret

data class PFunc(val selfHits: Long, val stackHits: Long, val name: String) {
	val module: String get() = name.substring(name.lastIndexOf(':') + 1)
	private val rawFilename: String? = name.lastIndexOf(':').let {
		if (it == -1) null else name.substring(0 until it)
	}

	val filename: String
		get() {
			val fn = rawFilename ?: return "Native"
			if (fn.contains('\n') || !fn.endsWith(".lu"))
				return "[script source]"

			return fn + "a"
		}

	val tidyName: String get() = "$filename:$module"

	val isNative: Boolean get() = rawFilename == null
}

fun load(file: File): ArrayList<PFunc> {
	RandomAccessFile(file, "r").use { fi ->
		val version = fi.readInt()
		check(version == 1)

		val count = fi.readLong()
		val functions = ArrayList<PFunc>(count.toInt())

		for (i in 1..count) {
			val selfHits = fi.readLong()
			val stackHits = fi.readLong()

			val nameLen = fi.readInt()
			val nameBytes = ByteArray(nameLen)
			fi.read(nameBytes)
			val name = String(nameBytes, Charsets.UTF_8)

			functions += PFunc(selfHits, stackHits, name)
		}

		return functions
	}
}

class ProfileFileFilter : FileFilter() {
	override fun accept(f: File): Boolean = f.isDirectory || f.name.endsWith(".spd")
	override fun getDescription(): String = "Profiling Data"
}

fun update(data: ArrayList<PFunc>?, ui: ProfilerUI) {
	if (data == null) {
		ui.outputArea.text = "No file loaded - please open a file"
		return
	}

	if (ui.sortMode.selectedIndex == 0)
		data.sortByDescending { it.stackHits }
	else
		data.sortByDescending { it.selfHits }

	val showIdx = ui.functionType.selectedIndex
	val showLua = showIdx == 0 || showIdx == 1
	val showNative = showIdx == 0 || showIdx == 2

	val sb = StringBuilder()

	sb.append("Self    Stack   Name\n")
	for (f in data) {
		val show = if (f.isNative) showNative else showLua
		if (!show)
			continue

		sb.append(String.format("%6d  %6d  %s\n", f.selfHits, f.stackHits, f.tidyName))
	}

	ui.outputArea.text = sb.toString()

	ui.outputScroll.validate()
}

fun main(args: Array<String>) {
	val ui = ProfilerUI()

	val min = Dimension(ui.frame.contentPane.minimumSize)
	min.width += 20
	min.height += 20
	ui.frame.minimumSize = min

	val caret = ui.outputArea.caret as DefaultCaret
	caret.updatePolicy = DefaultCaret.NEVER_UPDATE

	val chooser = JFileChooser(File(".").absoluteFile)

	var data: ArrayList<PFunc>? = null

	update(data, ui)

	chooser.fileFilter = ProfileFileFilter()

	ui.selectFileButton.addActionListener {
		val result = chooser.showOpenDialog(ui.frame)

		if (result != JFileChooser.APPROVE_OPTION)
			return@addActionListener

		ui.filePathLabel.text = chooser.selectedFile.absolutePath

		data = load(chooser.selectedFile)

		update(data, ui)
	}

	ui.sortMode.addActionListener { update(data, ui) }
	ui.functionType.addActionListener { update(data, ui) }
}
